import React from 'react';
import ReactDOM from 'react-dom';
import './stylesheets/index.css';
import App from './App';

import { Fabric } from 'office-ui-fabric-react/lib/Fabric'

import { BrowserRouter } from 'react-router-dom';
// import { Provider } from 'react-redux';
// import configureStore from './config/configureStore';

// const store = configureStore();
// console.log(store.getState());

// store.dispatch();

ReactDOM.render(
  // <Provider store={store}>
    <BrowserRouter>
      <Fabric>
        <App />
      </Fabric>
    </BrowserRouter>
  // </Provider>
  , document.getElementById('root')
);
