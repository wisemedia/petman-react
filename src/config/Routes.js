import React from 'react';
import { Router, Switch } from 'react-router-dom';
import history from '../helpers/history';
import { AppRoute, PrivateRoute } from '../helpers/RouterHelper';

import GridLayout from '../layouts/GridLayout';
import CleanLayout from '../layouts/CleanLayout';

import NotFoundPage from '../views/NotFoundPage.jsx';
import MainPage from '../views/MainPage.jsx';
import ClinicPage from '../views/ClinicPage.jsx';
import PetOwnerPage from '../views/PetOwnerPage.jsx';
import VetPage from '../views/VetPage.jsx';
import LoginPage from '../views/LoginPage';


const errorMessage = <p>Another wonderer lost in da mist<br />¯\_(ツ)_/¯</p>

const Routes = (session) => (
  <Router history={history}>
    <Switch>
      <AppRoute exact path='/login' layout={GridLayout} component={LoginPage} />
      <AppRoute exact path='/' layout={GridLayout} component={MainPage} session={session} />

      <PrivateRoute session={session} exact path='/clinic' layout={GridLayout} component={ClinicPage} />
      <AppRoute exact path='/pet_owner' layout={GridLayout} component={PetOwnerPage} />
      <AppRoute exact path='/veterinarian' layout={GridLayout} component={VetPage} />
      
      <AppRoute layout={CleanLayout} component={NotFoundPage} errorCode={'404'} errorText={errorMessage} />
    </Switch>
  </Router>
)

export default Routes;
