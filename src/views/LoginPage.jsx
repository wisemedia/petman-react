import React, { Component } from 'react';
import { Stack, IStackProps, TextField } from 'office-ui-fabric-react';
import { Label, ILabelStyles } from 'office-ui-fabric-react/lib/Label';
import { Pivot, PivotItem } from 'office-ui-fabric-react/lib/Pivot';
import { IStyleSet } from 'office-ui-fabric-react/lib/Styling';
import GoogleLogin from 'react-google-login';

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      passwordHash: ''
    };
  }

  handleChange = input => (event) => {
    this.setState({ [input]: event.target.value })
  }
  handleSubmit = (event) => {
    if (!event.target.checkValidity()) {
      event.preventDefault();
      return this.setState({ hasError: true})
    }
    event.preventDefault();
    this.setState({ hasError: false })
  }
  
  render() {
    const { username, passwordHash } = this.state;
    const responseGoogle = (response) => {
      console.log(response);
      this.props.actions.googleLoginUser(response);
    }

    return (
      <Stack>
        <Stack.Item align="center" >
          <Pivot>
            <PivotItem headerText="signup" headerButtonProps={{ 'data-order': 1, 'data-title': 'My Files Title' }}>
              <Stack tokens={{ childrenGap: 50 }} styles={{ root: { width: 150 } }} >
                <h2>registeeri</h2>
                <TextField label="username:" underlined value={username} onChange={this.handleChange('firstname')} />
                <TextField label="password:" underlined value={passwordHash} onChange={this.handleChange('lastname')} />
                <TextField label="username:" underlined value={username} onChange={this.handleChange('email')} />
                <TextField label="password:" underlined value={passwordHash} onChange={this.handleChange('passwordHash')} />
              </Stack>
            </PivotItem>
            <h1>LOGIN WITH GOOGLE</h1>

            <PivotItem headerText="Recent">
              <Stack tokens={{ childrenGap: 50 }} styles={{ root: { width: 650 } }} >
                <GoogleLogin
                  clientId="" //CLIENTID NOT CREATED YET
                  buttonText="LOGIN WITH GOOGLE"
                  onSuccess={responseGoogle}
                  onFailure={responseGoogle}
                />
                <TextField label="username:" underlined value={username} onChange={this.handleChange('username')} />
                <TextField label="password:" underlined value={passwordHash} onChange={this.handleChange('passwordHash')} />
              </Stack>
            </PivotItem>

          </Pivot>
        </Stack.Item>
      </Stack>
    )
  }
}

export default LoginPage;
