import React, { Component } from 'react'

class NotFound extends Component {
  render() {
    const { errorCode, errorText, children } = this.props;
    return(
      <div id='errorPage' >
        <h2>
          {errorCode}
        </h2>
        <p>
          <hr />
          {errorText}
        </p>
        {children}
      </div>
    )
  }
}

export default NotFound;
