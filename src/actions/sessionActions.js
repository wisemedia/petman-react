import * as types from './actionTypes';
import axios from 'axios';
import history from '../helpers/history';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

function loginSuccess(response) {
  return { type: types.LOG_IN_SUCCESS, response }
}

function loginFailed() {
  return { type: types.LOGIN_FAIL }
}

function setCurrentUser(currentUser) {
  return { type: types.USER_DATA_OBTAINED, currentUser }
}

export function logOut() {
  localStorage.removeItem('user');
  history.push('/login');
  window.location.reload(true);
  return { type: types.LOG_OUT }
}

export function googleLoginUser(response) {
  console.log(response)
  return function (dispatch) {
    const user = {user: true}
    localStorage.setItem('user', JSON.stringify(user.data))
    history.push('/');
    window.location.reload(true);
    dispatch(loginSuccess(response));
    dispatch(setCurrentUser(user));
  };
}

export function logInUser(credentials) {
  const requestOptions = {
    username: credentials.username,
    passwordHash: credentials.passwordHash
  }
  
  return function (dispatch) {
    axios.post('http://localhost:8080/login', requestOptions)
      .then(response => {
        const authToken = response.headers.authorization;
        axios.get('http://localhost:8080/', {
          headers: { 'Authorization': authToken }
        }).then(user => {
          localStorage.setItem('user', JSON.stringify(user.data))
          history.push('/');
          window.location.reload(true);
          dispatch(loginSuccess(response));
          dispatch(setCurrentUser(user));
        })
      }).catch(error => {
        console.log(error);
        dispatch(loginFailed(error));
      });
  }
}
