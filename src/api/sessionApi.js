import axios from 'axios';

class SessionApi {
  static login(credentials) {
    console.log(credentials);
    axios.post('http://localhost:8080/login', {
      username: credentials.username,
      passwordHash: credentials.passwordHash
    })
    .then(function(response) {
      console.log(response);
      return response;
    })
    .catch(function(error) {
      console.log(error);
      return error;
    })
  }
}

export default SessionApi;
