class UsersApi {
  static requestHeaders() {
    return { 'Authorization': `Bearer ${sessionStorage.jwt}` }
  }

  static getAllUsers() {
    const headers = this.requestHeaders();
    const request = new Request('ADD URL HIIR', {
      method: 'GET',
      headers: headers
    });

    return fetch(request).then(response => {
      return response.json();
    }).catch(error => {
      return error;
    })
  }
}

export default UsersApi;
