import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function tokenValidationReducer(state = initialState.token_validated, action) {
  switch (action.type) {
    case types.TOKEN_VALIDATION_SUCCESS:
      sessionStorage.setItem('validated', true)
      return {
        ...state,
        token_validated: true
      }
    case types.TOKEN_VALIDATION_FAILED:
      sessionStorage.setItem('validated', false);
      return {
        ...state,
        token_validated: false
      }
    default:
      return state;
  }
}
