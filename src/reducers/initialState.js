export default {
  session: !!sessionStorage.auth,
  token_validated: !!sessionStorage.validated
}
