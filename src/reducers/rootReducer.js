import { combineReducers } from 'redux';
import session from './sessionReducer';
import tokenValidation from './tokenValidationReducer';

const rootReducer = combineReducers({
  session,
  // tokenValidation,
})

export default rootReducer;
