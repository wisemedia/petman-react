import * as types from '../actions/actionTypes';
// import {session, currentUser} from './initialState';
import axios from 'axios';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { session: !!sessionStorage.auth, user } : {};

export default function sessionReducer(state = initialState, action) {
  switch (action.type) {
    case type.GOOGLE_LOG_IN_SUCCESS:
      console.log('yeeee google login');
      console.log(action);

      return {
        ...state,
        session: true
      }
    case types.LOG_IN_SUCCESS:
      console.log(action)
      const authToken = action.response.headers.authorization;
      axios.defaults.headers.common['Authorization'] = authToken;
      sessionStorage.setItem('auth', authToken);
      return {
        ...state.session,
        session: true,
        user: action.user,
      }
    case types.USER_DATA_OBTAINED:
      return {
        ...state,
        user: action.user
      }
    case types.LOG_OUT:
    case types.LOGIN_FAIL:
      axios.defaults.headers.common['Authorization'] = null;
      sessionStorage.removeItem('auth');
      return {
        ...state,
        session: false
      }
    default:
      return state;
  }
}
