import React from 'react';
import Routes from './config/Routes';
// import { connect } from 'react-redux';

function App() {
  return (
    <Routes />
  );
}

// const mapStateToProps = (state) => {
//   return {};
// };

// const mapDispatchToProps = (dispatch) => {
//   return {};
// }

// export default connect(mapStateToProps, mapDispatchToProps)(App);

export default App;
