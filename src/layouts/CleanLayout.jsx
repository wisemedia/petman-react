import React from 'react';

export const CleanLayout = (props) => (
  <div className='container'>
    {props.children}
  </div>
)

export default CleanLayout;