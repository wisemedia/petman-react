import React from 'react';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';

const grid = {
  display: 'grid',
  gridTemplateColumns: '1fr 3fr',
  gridTemplateRows: '3fr 1fr',
  gridTemplateAreas: '"header main" "footer footer"',
}
const MainLayout = (props) => (
  <div className='grid-container' style={grid}>
    <header style={{ gridArea: 'header' }}>
      <Navbar />
    </header>
    <footer style={{ gridArea: 'footer' }}>
      <Footer />
    </footer>
    <main style={{ gridArea: 'main' }}>
      {props.children}
    </main>
  </div>
)

export default MainLayout;