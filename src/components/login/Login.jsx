
import React, { Component } from 'react';
import './App.css';
import GoogleLogin from 'react-google-login';

class Login extends Component {
  handleSubmit = (event) => {
    if (!event.target.checkValidity()) {
      event.preventDefault();
      return this.setState({ hasError: true })
    }
    event.preventDefault();
    this.setState({ hasError: false })
    this.props.actions.logInUser(this.state.credentials);
  }

  render() {
    const responseGoogle = (response) => {
      console.log(response);
      this.props.actions.googleLoginUser(response)
    }

    return (
      <div className="App">
        <h1>LOGIN WITH GOOGLE</h1>
        <GoogleLogin
          clientId="" //CLIENTID NOT CREATED YET
          buttonText="LOGIN WITH GOOGLE"
          onSuccess={responseGoogle}
          onFailure={responseGoogle}
        />
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(sessionActions, dispatch)
  };
}

export default connect(null, mapDispatchToProps)(Login);
