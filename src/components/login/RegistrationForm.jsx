import React, { Component } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import * as invitationActions from '../../actions/invitationActions';
import { connect } from 'react-redux';
import TextInput from '../TextInput';
import NotFound from '../error/NotFound';
import { Row, Col } from 'reactstrap';
import picture from '../../assets/almost_there_et.svg';
import testpic from '../../assets/test.svg';
import FormInput, { TheInput } from '../FormInput';
import FormContainer from '../form/FormContainer';


class RegistrationForm extends Component {
  constructor(props) {
    super(props);
    const { token } = this.props.match.params;
    this.state = {
      hasError: false,
      fireRedirect: false,
      submitted: false,
      invitation_form_data: {
        firstName: '',
        lastName: '',
        password: '',
        token: token
      }
    };
  }

  componentDidMount() {
    const invitation_form_data = this.state;
    this.props.actions.validateToken(invitation_form_data)
  }

  submitHandler = (event) => {
    if (!event.target.checkValidity()) {
      event.preventDefault();
      return this.setState({ hasError: true });
    }
    this.setState({ submitted: true, hasError: false });
    this.props.actions.registerUser(this.state.invitation_form_data);
  }

  onChange = (event) => {
    const { name, value } = event.target;
    const { invitation_form_data } = this.state;
    return this.setState({
      invitation_form_data: {
        ...invitation_form_data,
        [name]: value
      }
    })
  }


  render() {
    const { token_validated } = this.props;
    const { fireRedirect, invitation_form_data, submitted, hasError } = this.state;
    const { from } = this.props.location.state || '/';

    return (
      <div className='page-wrapper'>
        <div className="registration">
          <span className='wrapper-image'><img src={testpic} alt='kohe kohal' /></span>
          {/* !token_validated ?
      <NotFound errorCode={'404'} errorText={"See link ei ole enam valiidne"}>
        <a href="/" className="btn btn-outline-brand rounded btn-block btn-lg">Mine avalehele</a>
      </NotFound>
      : */}
          <h2 className="headline text-center">Registreeri foorumi kasutajaks</h2>
          <Row>
            <Col>
              <form onSubmit={this.submitHandler} noValidate className={hasError ? 'has-error' : ''} >
                {/* <div className={'form-group' + (submitted && !invitation_form_data.firstName ? ' has-error' : '')}>
              <label htmlFor="firstName">Eesnimi</label>
                <input type="text" className="form-control" name="firstName" value={invitation_form_data.firstName} onChange={this.onChange} />
              {submitted && !invitation_form_data.firstName && <div className="help-block">First Name is required</div>}
            </div> */}
                <TextInput name="firstName" label="Eesnimi" value={invitation_form_data.firstName} onChange={this.onChange} required />
                <TextInput name="lastName" label="Perenimi" value={invitation_form_data.lastName} onChange={this.onChange} required />
                <TextInput name="password" label="Parool" type="password" value={invitation_form_data.password} onChange={this.onChange} required />
                <button className="btn btn-brand rounded btn-block btn-lg" type="submit">Registreeri kasutajaks</button>
              </form>
              <FormInput>
                <div name="sisu1" label="Eesnimi" value={invitation_form_data.firstName} onChange={this.onChange} required />
                <div name="sisu2" label="Perenimi" value={invitation_form_data.lastName} onChange={this.onChange} required />
                <div name="sisu3" label="Parool" type="password" value={invitation_form_data.password} onChange={this.onChange} required />
              </FormInput>
              <FormContainer>
                <div name="sisu1" title="Eesnimi" onChange={this.onChange} required
                  info='Your username must have more than 3 and fewer than 64 characters, including at least 1 letter.'/>
                <div name="sisu2" title="Perenimi" value={invitation_form_data.lastName} onChange={this.onChange} required
                  info='Must have more than 8 characters, including at least 1 letter and number.' />
                <div name="sisu3" title="Parool" value={invitation_form_data.lastName} onChange={this.onChange} required
                  info='Must have more than 8 characters, including at least 1 letter and number.' />
                <div name="sisu4" title="E-mail" value={invitation_form_data.lastName} onChange={this.onChange} required
                  info='You’ll need access to this email address to verify your account.' />
              </FormContainer>
            </Col>
          </Row>

          {fireRedirect && (
            <Redirect to={from || '/login'} />
          )}
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return state.tokenValidation
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(invitationActions, dispatch)
  };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RegistrationForm));
