import React from 'react';

export default class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      term: '',
      autoCompleteResults: [],
      itemSelected: {},
      showItemSelected: false
    };

    // https://petman-admin.wisemedia.ee/search.json?phrase

    $.getJSON('http://localhost:8083/search.json?' + this.state.term)
      .then(response => this.setState({ autoCompleteResults: response.items }))
  }

  getAutoCompleteResults = (e) => {
    this.setState({
      term: e.target.value
    }, () => {
      $.getJSON('http://localhost:8083/search.json?' + this.state.term)
        .then(response => this.setState({ autoCompleteResults: response.items }))
    });
  }

  render() {
    let autoCompleteList = this.state.autoCompleteResults.map((response, index) => {
      return <div key={index}>
        <h2>{response.title}</h2>
        <p>{response.description}</p>
      </div>
    });
    return (
      <div>
        <input ref={(input) => { this.searchBar = input }}
          value={this.state.term}
          onChange={this.getAutoCompleteResults}
          type='text'
          placeholder='Search...' />
          { autoCompleteList }
      </div>
    );
  }
}