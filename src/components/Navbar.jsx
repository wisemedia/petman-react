import React, { Component } from 'react';
import { Nav } from 'office-ui-fabric-react/lib/Nav';

export default class NavBar extends Component {
  render() {
    return (
      <Nav styles={{ root: { width: 300 } }}
        expandButtonAriaLabel="Expand or collapse"
        groups={
          [
            {
              name: 'Clinic',
              links: [
                {
                  key: 'Dogs',
                  name: 'Create new account',
                  url: '#'
                },
                {
                  key: 'Cats',
                  name: 'Storage',
                  url: '#'
                },
                {
                  key: 'Cows',
                  name: 'Calender/Appointment',
                  url: '#'
                }
              ]
            },
            {
              name: 'Owners',
              links: [
                {
                  key: 'petOwners',
                  name: 'My pets',
                  url: '#'
                },
                {
                  key: 'Search',
                  name: 'Search',
                  url: '#'
                },
                {
                  key: 'Food supplements',
                  name: 'Food supplements',
                  url: '#'
                }
              ]
            },
            {
              name: 'Vet',
              links: [
                {
                  key: 'Search',
                  name: 'Search',
                  url: '#'
                },
                {
                  key: 'Breed infos',
                  name: 'Breed infos',
                  url: '#'
                },
                {
                  key: 'Something',
                  name: 'Something',
                  url: '#'
                }
              ]
            }
          ]}
      />
    );
  }
}
