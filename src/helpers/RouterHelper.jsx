import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
  <Route {...rest} render={props => (
    <Layout>
      <Component {...rest} {...props} />
    </Layout>
  )} />
);

export const PrivateRoute = ({ component: Component, layout: Layout, session, ...rest }) => (
  <Route {...rest} render={props =>
      session === true && localStorage.getItem('user') ? (
        <Layout>
          <Component {...props} />
        </Layout>
      ) : (
          <Redirect to="/unauthorized" />
        )
    }
  />
);
